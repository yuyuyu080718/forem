module Admin
  class WelcomeController < Admin::ApplicationController
    layout "admin"

    def index
      @daily_threads = Article.where("title LIKE 'Welcome Thread - %'")
    end

    def create
      welcome_thread = Article.create(
        body_markdown: welcome_thread_content,
        user: User.staff_account,
      )
      redirect_to "#{URI.parse(welcome_thread.path).path}/edit"
    end

    private

    def welcome_thread_content
      I18n.t("admin.welcome_controller.title_welcome_thread_v0_p",
             settings_community_communi: ::Settings::Community.community_name)
    end
  end
end
